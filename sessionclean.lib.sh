sapi_get_extension_config () {
    configPath="$1"
    shift

    php -c "$configPath" "${DIR}/sapi_get_extension_config.php" "$@"
}

extract_value () {
    config="$1"
    # TODO replace . -> \.
    key="$2"

    echo "$config" | sed -ne "s/^${key}=\\(.*\\)$/\\1/p" -e "s/\n*$//"
}
