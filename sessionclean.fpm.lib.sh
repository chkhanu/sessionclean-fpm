fpm_pool_get_extension_config () {
    php -n "${DIR}/fpm_pool_get_extension_config.php" "$@"
}

fpm_pool_session_path_and_lifetime () {
    pool_conf="$1"
    # export save_path gc_maxlifetime

    _config=$(fpm_pool_get_extension_config "$pool_conf" session)

    _save_handler=$(extract_value "$_config" session.save_handler)
    [ -n "$_save_handler" ] && [ "$_save_handler" != "files" ] && continue

    _save_path=$(extract_value "$_config" session.save_path)
    _save_path=${_save_path:-${save_path}}

    [ "$_save_path" != "$save_path" ] || continue
    [ -d "$_save_path" ] || continue

    _gc_maxlifetime=$(extract_value "$_config" session.gc_maxlifetime)
    if [ -n "$_gc_maxlifetime" ]; then
        _gc_maxlifetime=$(($_gc_maxlifetime/60))
    else
        _gc_maxlifetime=$gc_maxlifetime
    fi

    printf "%s:%s\n" "$_save_path" "$_gc_maxlifetime"
}
