#!/usr/bin/php -n
<?php
ini_set('error_reporting', ~E_ALL);

// Extension section, like 'session' in 'session.gc_maxlifetime'
$ext = count($argv) > 1 ? $argv[1] : null;
$extDot = $ext ? ($ext . '.') : null;

foreach(ini_get_all($ext) as $k => $v)
    echo ("{$k}={$v['local_value']}\n");
