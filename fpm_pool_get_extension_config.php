#!/usr/bin/php -n
<?php
ini_set('error_reporting', ~E_ALL);

// Path to the pool config file
$configPath = $argv[1];

// Extension section, like 'session' in 'session.gc_maxlifetime'
$ext = count($argv) > 2 ? $argv[2] : null;
$extDot = $ext ? ($ext . '.') : null;

//

$config = parse_ini_file($configPath, true);

if (false === $config) {
    exit(1);
}

$pool = array_keys($config);
$pool = $pool[0];

$config = array_shift($config);

foreach (array('php_admin_flag', 'php_admin_value') as $section) {
    if (!isset($config[$section])) {
        continue;
    }

    foreach ($config[$section] as $key => $value) {
        if (!$ext || !strcasecmp(substr($key, 0, strlen($extDot)), $extDot)
        ) {
            // TODO fix escaped $ situation
            $value = str_replace('$pool', $pool, $value);
            echo("{$key}={$value}\\n");
        }
    }
}

exit(0);
