# README #

### What is this repository for? ###

#### Quick summary
This is a modified Debian sessionclean script, used to run cleanups of php file sessions on php-fpm pools.

It will proceed on any pool, which config differs from fpm defaults (has php_admin_value's session.gc_maxlifetime session.save_path set).

#### Version
Some beta.

### How do I get set up? ###

Just place it somewhere in /usr/local/lib/php5/
and add cron job.